<?php
// $Id$

/**
 * @file
 * Admin functions for the CONTENTdm Organic Groups Integration module.
 */

/**
 * Generate the admin settings form.
 */
function contentdm_og_admin_settings() {

  $form['contentdm_og_widget'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display \'Post in group\' widget on'),
    '#collapsible' => FALSE,
    );

  $form['contentdm_og_widget']['contentdm_og_display_fullnode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full node'),
    '#default_value' => variable_get('contentdm_og_display_fullnode', 1),
  );

  $form['contentdm_og_widget']['contentdm_og_display_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Node teaser'),
    '#default_value' => variable_get('contentdm_og_display_teaser', 0),
  );

  $form['contentdm_og_widget']['contentdm_og_display_search_result'] = array(
    '#type' => 'checkbox',
    '#title' => t('CONTENTdm search result'),
    '#default_value' => variable_get('contentdm_og_display_search_result', 1),
  );


  return system_settings_form($form); 
}
